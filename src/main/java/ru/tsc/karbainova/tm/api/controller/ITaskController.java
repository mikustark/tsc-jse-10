package ru.tsc.karbainova.tm.api.controller;

public interface ITaskController {
    void showTask();

    void clearTask();

    void createTask();
}
