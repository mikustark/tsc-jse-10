package ru.tsc.karbainova.tm.repository;

import ru.tsc.karbainova.tm.api.repository.ITaskRepository;
import ru.tsc.karbainova.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tascks = new ArrayList<>();

    @Override
    public void add(Task task) {
        tascks.add(task);
    }

    @Override
    public void remove(Task task) {
        tascks.remove(task);
    }

    public List<Task> findAll() {
        return tascks;
    }

    ;

    @Override
    public void clear() {
        tascks.clear();
    }
}
