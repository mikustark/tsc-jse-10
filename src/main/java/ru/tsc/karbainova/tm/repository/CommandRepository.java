package ru.tsc.karbainova.tm.repository;

import ru.tsc.karbainova.tm.api.repository.ICommandRepository;
import ru.tsc.karbainova.tm.constant.ArgumentConst;
import ru.tsc.karbainova.tm.constant.TerminalConst;
import ru.tsc.karbainova.tm.model.Command;

public class CommandRepository implements ICommandRepository {
    public static Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null, "Exit program."
    );
    public static Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.HELP, "Display list of terminal commands."
    );
    public static Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.VERSION, "Display program version."
    );
    public static Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.INFO, "Display info."
    );
    public static Command COMMANDS = new Command(
            TerminalConst.CMD_COMMANDS, ArgumentConst.COMMANDS, "Display commands."
    );
    public static Command ARGUMENTS = new Command(
            TerminalConst.CMD_ARGUMENTS, ArgumentConst.ARGUMENTS, "Display arguments."
    );
    public static Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ABOUT, "Display developer info."
    );
    public static Command CREATE_PROJECT = new Command(
            TerminalConst.CREATE_PROJECT, null, "Create project."
    );
    public static Command LIST_PROJECT = new Command(
            TerminalConst.LIST_PROJECT, null, "List project."
    );
    public static Command CLEAR_PROJECT = new Command(
            TerminalConst.CLEAR_PROJECT, null, "Clear project."
    );
    public static Command CREATE_TASK = new Command(
            TerminalConst.CREATE_TASK, null, "Create task."
    );
    public static Command LIST_TASK = new Command(
            TerminalConst.LIST_TASK, null, "List task."
    );
    public static Command CLEAR_TASK = new Command(
            TerminalConst.CLEAR_TASK, null, "Clear task."
    );

    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            EXIT, HELP, VERSION, INFO, ABOUT, ARGUMENTS, COMMANDS,
            CLEAR_PROJECT, CREATE_PROJECT, LIST_PROJECT,
            CLEAR_TASK, CREATE_TASK, LIST_TASK
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }
}
