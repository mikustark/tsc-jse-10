package ru.tsc.karbainova.tm.util;

import java.util.Scanner;

public interface TerminalUtil {
    Scanner scanner = new Scanner(System.in);

    static String nextLine() {
        return scanner.nextLine();
    }
}
