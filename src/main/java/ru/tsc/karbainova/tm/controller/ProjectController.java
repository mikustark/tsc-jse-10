package ru.tsc.karbainova.tm.controller;

import ru.tsc.karbainova.tm.api.controller.IProjectController;
import ru.tsc.karbainova.tm.api.service.IProjectService;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectServices;

    public ProjectController(final IProjectService projectServices) {
        this.projectServices = projectServices;
    }

    @Override
    public void showProject() {
        final List<Project> projects = projectServices.findAll();
        for (Project project : projects) System.out.println(project);
    }

    @Override
    public void clearProject() {
        System.out.println("[CLEAR PROJECTS]");
        projectServices.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        projectServices.create(name, description);
        System.out.println("[OK]");
    }
}
